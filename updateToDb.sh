export JAVA_HOME=/app/oracle/products/java
export PATH=$JAVA_HOME/bin:$PATH

/app/liquibase/liquibase --defaultsFile=/app/liquibase/liquibase.properties --changeLogFile=/app/liquibase/dbChangelog.xml update

